function register_listeners(document) {
  document.getElementById('tab_control_button_send').addEventListener('click', tab_control_button_send_onClick);
  document.getElementById('tab_control_direction').addEventListener('change', tab_control_direction_change);

  document.getElementById('tab_interrupt_button_send').addEventListener('click', tab_interrupt_button_send_onClick);
  document.getElementById('tab_interrupt_direction').addEventListener('change', tab_interrupt_direction_change);
  document.getElementById('tab_interrupt_direction').dispatchEvent(new Event('change'));

  document.getElementById('tab_interrupt_interface').addEventListener('change', tab_interrupt_interface_change);
  document.getElementById('tab_interrupt_endpoint').addEventListener('change', tab_interrupt_endpoint_change);

  var collection = document.getElementsByClassName('tooltipper');
  for (var i = 0; i<collection.length; i++) {
    var element = collection[i];
    element.addEventListener('mouseover', show_tooltip);
    element.addEventListener('mouseout', hide_tooltip);
  }

  // TODO: dump top.opener calls. Everywhere.
  top.opener.notifyDeviceConnected = function () {
    document.getElementById('connect_device_button').disabled = 1;
    document.getElementById('disconnect_device_button').disabled = 0;
    document.getElementById('device_indicator').className="connected";
    document.getElementById('device_status_label').innerHTML = 'connected';
  }
  top.opener.notifyDeviceDisconnected = function () {
    document.getElementById('connect_device_button').disabled = 0;
    document.getElementById('disconnect_device_button').disabled = 1;
    document.getElementById('device_indicator').className="disconnected";
    document.getElementById('device_status_label').innerHTML = 'disconnected';
  }

  top.opener.notifyDeviceConfiguration = function (config) {
    parseDeviceConfiguration(config);
  }

  document.getElementById('connect_device_button').addEventListener('click',function() {
      top.opener.connectDigiUSB();
    }
  );
  document.getElementById('disconnect_device_button').addEventListener('click',function() {
      top.opener.disconnectDigiUSB();
    }
  );
}

function prepareControlTransferParams() {
  var result = {
    direction: document.getElementById("tab_control_direction").value,
    requestType: document.getElementById("tab_control_request_type").value,
    recipient: document.getElementById("tab_control_recipient").value,
    request: parseInt(document.getElementById("tab_control_request").value) || 0,
    value: parseInt(document.getElementById("tab_control_value").value) || 0,
    index: parseInt(document.getElementById("tab_control_index").value) || 0
  };

  if (result.direction == 'out') {
    result.data = buildDataFromString(document.getElementById("tab_control_data").value, 'control');
    if (result.data == null) {
      return null;
    }
  } else {
    result.length = parseInt(document.getElementById("tab_control_length").value) || 0;
  }

  return result;
}

function prepareInterruptTransferParams() {
  var result = {
    direction: document.getElementById("tab_interrupt_direction").value,
    interface: parseInt(document.getElementById("tab_interrupt_interface").value) || 0,
    endpoint: parseInt(document.getElementById("tab_interrupt_endpoint").value) || 0,
    timeout: parseInt(document.getElementById("tab_interrupt_timeout").value) || 0
  };

  if (result.direction == 'out') {
    result.data = buildDataFromString(document.getElementById("tab_interrupt_data").value, 'interrupt');
    if (result.data == null) {
      return null;
    }
  } else {
    result.length = parseInt(document.getElementById("tab_interrupt_length").value) || 0;
  }

  return result;
}

// TODO: skip top.opener usage.
function tab_control_button_send_onClick(event) {
  var params = prepareControlTransferParams();

  if (params == null) {
    return null;
  }

  event.srcElement.disabled = 1;

  top.opener.launchControlTransfer(
    params,
    function (result) {
      event.srcElement.disabled = 0;

      if (result.resultCode != 0) {
        message_popup("Control transfer failed, result code "+result.resultCode);
      } else {
        if (document.getElementById("tab_control_direction").value == 'in') {
          var data = buildDataPresentation(result.data, 'control');
          document.getElementById("tab_control_result").value = data;
        }
      }
    },
    function (err) {
      event.srcElement.disabled = 0;
      message_popup(err)
    }
  );
}

// TODO: skip top.opener usage.
function tab_interrupt_button_send_onClick(event) {
  var params = prepareInterruptTransferParams();

  if (params == null) {
    return null;
  }

  event.srcElement.disabled = 1;

  top.opener.launchInterruptTransfer(
    params,
    function (result) {
      event.srcElement.disabled = 0;

      if (chrome.runtime.lastError) {
        message_popup(chrome.runtime.lastError.message);
        log(chrome.runtime.lastError);
      } else if (result.resultCode != 0) {
        message_popup("Interrupt transfer failed, result code "+result.resultCode);
      } else {
        if (document.getElementById("tab_interrupt_direction").value == 'in') {
          var data = buildDataPresentation(result.data, 'interrupt');
          document.getElementById("tab_interrupt_result").value = data;
        }
      }
    },
    function (err) {
      event.srcElement.disabled = 0;
      message_popup(err)
    }
  );
}


function tab_control_direction_change(event) {
  if (event.srcElement.value == 'out') {
    document.getElementById('tab_control_length').disabled = 1;
    document.getElementById('tab_control_data').disabled = 0;
  } else {
    document.getElementById('tab_control_length').disabled = 0;
    document.getElementById('tab_control_data').disabled = 1;
  }
}

function tab_interrupt_direction_change(event) {
  if (event.srcElement.value == 'out') {
    document.getElementById('tab_interrupt_length').disabled = 1;
    document.getElementById('tab_interrupt_data').disabled = 0;
  } else {
    document.getElementById('tab_interrupt_length').disabled = 0;
    document.getElementById('tab_interrupt_data').disabled = 1;
  }
}

function buildDataPresentation( ab, mode ) {
  var resultType = getRadioValue('tab_'+mode+'_result_format');

  var byteArray = new Uint8Array(ab);

  var result = '';

  for (var i = 0; i< byteArray.length; i++) {

    if (resultType == 'string') {
      result += String.fromCharCode(byteArray[i]);
    } else {
      if (i != 0) {
        result += ','
      }
      if (resultType == 'dec') {
        result += byteArray[i];
      } else if (resultType == 'hex') {
        result += byteArray[i].toString(16);
      }
    }

  }

  return result
}


function buildDataFromString ( string , mode) {

  var buildType = getRadioValue('tab_'+mode+'_data_format');

  if (buildType == 'string') {
    if (string.length > 8) {
      message_popup("String cannot be longer than 8 characters.");
      return null;
    } else {
      var data = new Uint8Array(string.length);
      for (var i = 0; i<string.length; i++) {
        data[i] = string.charCodeAt(i);
      }
      return data.buffer;
    }
  } else if (buildType == 'dec') {
    var byteVals = string.split(',');
    if(!string) {
      byteVals = [];
    }
    if (byteVals.length > 8) {
      message_popup("Well, 8 bytes is maximum. No more. You got "+byteVals.length+".");
    } else {
      var data = new Uint8Array(byteVals.length);
      for (var i = 0; i<byteVals.length; i++) {
        byteVals[i] = parseInt(byteVals[i]);
        if (0 <= byteVals[i] && byteVals[i] <= 255) {
          data[i] = byteVals[i];
        } else {
          message_popup("It's pretty uncommon, to have byte value of "+byteVals[i]+". In most cases the value is between 0 and 255 (inclusive)");
        }
      }
      return data.buffer;
    }
  }
}

function getRadioValue(name) {
  var value = undefined;
  var elements = document.getElementsByName(name);
  for (var i = 0; i<elements.length; i++) {
    if (elements[i].checked) {
      value = elements[i].value;
      break;
    }
  }
  return value;

}

function message_popup(msg) {
  var box = document.getElementById('popup_div');
  var div = document.createElement('div');
  div.innerHTML = msg;
  box.appendChild(div);
  box.style.display = '';
  setTimeout(function() { box.removeChild(div); if (!box.childNodes.length){box.style.display = 'none'} }, 4000);
}

function show_tooltip(event) {
  var element = event.srcElement;
  var tooltip = document.getElementById('tooltip_div');
  tooltip.style.left = event.clientX+5+'px';
  tooltip.style.top = event.clientY+5+'px';
  tooltip.style.display = '';
  tooltip.innerHTML = element.dataset.tooltip;
}

function hide_tooltip(event) {
  var element = event.srcElement;
  var tooltip = document.getElementById('tooltip_div');
  tooltip.style.display = "none"
}

function setByPath(object, path, value) {
  path.forEach(function(pathElement, i, array) {
    if (array.length > i+1) {
      if (object[pathElement]) {
      } else {
        object[pathElement] = {};
      }
      object = object[pathElement];
    } else {
      object[pathElement] = value;
    }
  });

}

function parseDeviceConfiguration(config) {
  var definition = {};

  config.interfaces.forEach(function (interface) {
    interface.endpoints.forEach(
      function (endpoint) {
       endpoint;
       setByPath(definition,[endpoint.type,endpoint.direction,interface.interfaceNumber,(endpoint.address &= 0b1111)], endpoint);
      }
    );
  });

  top.DEVCONFIGPARSED = definition;

  tab_interrupt_apply_device_configuration(definition.interrupt);

}

function tab_interrupt_apply_device_configuration(config) {
  var select = document.getElementById('tab_interrupt_direction');
  Object.keys(config)

  var tabDOM = document.getElementById('tab_interrupt_transfer');
  tabDOM.dataset.interrupt = JSON.stringify(config);

  document.getElementById('tab_interrupt_direction').dispatchEvent(new Event('change'));

}

function tab_interrupt_direction_change(evt) {
  var direction = evt.srcElement.value;
  var config;
  try {
    config = JSON.parse(document.getElementById('tab_interrupt_transfer').dataset.interrupt);
  } catch (e) {
    config = null;
  }

  if (config) {
    if (direction == 'in') {
      document.getElementById('tab_interrupt_length').disabled = 0;
      document.getElementById('tab_interrupt_data').disabled = 1;
    } else {
      document.getElementById('tab_interrupt_length').disabled = 1;
      document.getElementById('tab_interrupt_data').disabled = 0;
    }

    var select = document.getElementById('tab_interrupt_interface');
    select.innerHTML = '';

    Object.keys(config[direction]||[]).forEach( function(interface) {
      var option = document.createElement('option');
      option.value = interface;
      option.text = interface;
      select.add(option);
    });

    select.dispatchEvent(new Event('change'));
  }
}

function tab_interrupt_interface_change() {
  var config = JSON.parse(document.getElementById('tab_interrupt_transfer').dataset.interrupt);
  var direction = document.getElementById('tab_interrupt_direction').value;
  var interface = document.getElementById('tab_interrupt_interface').value;

  var select = document.getElementById('tab_interrupt_endpoint');
  select.innerHTML = '';

  if (config[direction] && config[direction][interface]) {
    Object.keys(config[direction][interface] || []).forEach( function(endpoint) {
      var option = document.createElement('option');
      option.value = endpoint;
      option.text  = endpoint;
      select.add(option);
    });
  }
  select.dispatchEvent(new Event('change'));
}

function tab_interrupt_endpoint_change(evt) {
  var config = JSON.parse(document.getElementById('tab_interrupt_transfer').dataset.interrupt);

  var direction = document.getElementById('tab_interrupt_direction').value;
  var interface = document.getElementById('tab_interrupt_interface').value;
  var endpoint = document.getElementById('tab_interrupt_endpoint').value;

  if (direction && interface && endpoint) {
    var def = config[direction][interface][endpoint];
    document.getElementById('tab_interrupt_length').max = def.maximumPacketSize;
    document.getElementById('tab_interrupt_length').value = def.maximumPacketSize;
  }
}

document.getElementById('popup_div').innerHTML = '';
register_listeners(document);
log("attaching handlers");


function log(msg) {
 // if (top.console && top.console.log) { console.log(msg, arguments)}
};
