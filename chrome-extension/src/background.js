
chrome.app.runtime.onLaunched.addListener(function() {



    var w = chrome.app.window.create('window.html',
    {
        'outerBounds': {
          'width': 800,
          'height': 500
        }
       ,id: "USB_WINDOW_MAIN"
    }, function (win) {
        log(win);
        win.onClosed.addListener(function() {
            log('closed');
        });

    win.contentWindow.onLoad = function () {
      log("INHERE!", arguments);
    }
    });

    return;

});


// And this is here to clear hDevice when device is gone.
chrome.usb.onDeviceRemoved.addListener(
    function(device) {
      if (top.notifyDeviceDisconnected) {
        top.notifyDeviceDisconnected();
      }
      top.hDevice = undefined;
      log("DEV Removed: ", device)
    }
)

// This is listening part for the event sent from outer space.
chrome.runtime.onMessageExternal.addListener(
  function(request, sender, sendResponse) {
    if (request['type'] == 'sendByte') {
        sendByte(request['value']);
    } else if (request['type'] == 'readByte') {

        receiveByte(function (byteValue) {
                log("BV: ", byteValue);
                sendResponse({byteValue: byteValue});
            });
        // will be willing to send response asynchronously. THIS IS A MUST HAVE, as
        // we will use sendResponse in another callback.
        return true;

    }

});

function disconnectDigiUSB() {
  var device;
  if (device = getUsbDevice()) {
    chrome.usb.closeDevice(device, function () {
      chrome.usb.onDeviceRemoved.dispatch(device);
    } );
  }
}

// TODO: change "Use Top Pollutor Template" to something better. Eventually. :)
// This is connect to digispark function. It gets devices that are accessible for us
// and calls callback, if one wants. It connects to "just first device". Remember.
function connectDigiUSB(callback, onerror) {
    log("Connecting digispark. Maybe.");


    chrome.usb.getDevices({} , function(devices) {
        top.device = devices[0];
        log("Device: "+top.device, devices);

        if (!top.device) {
            log("No device located to connect to. Check if device is connected.")
            if (onerror) {
                onerror("No device to connect to. Check if device is connected.")
            }
        } else {
            chrome.usb.openDevice(top.device, function (handle) {


                top.hDevice = handle;
                log("Device handle: "+top.hDevice, arguments)
                if (top.notifyDeviceConnected) {
                  top.notifyDeviceConnected();
                };

                chrome.usb.getConfiguration(getUsbDevice(), function (config) {
                  if (top.notifyDeviceConfiguration) {
                    top.notifyDeviceConfiguration(config);
                  };
                });

                if (callback) {
                  callback();
                }
            } )
        }

    })
}


// TODO: Reimplement usb code (details below)
// Remove sendByte / receiveByte methods, as those was implemented for DigiUSB
// library.
// Add methods for launching transfer in somewhat more sane way.
function getUsbDevice() {
  return top.hDevice;
}

function launchControlTransfer(hTransferParams, callback, onerror) {
  var callAction = function () {
    chrome.usb.controlTransfer(
      getUsbDevice(),
      hTransferParams,
      callback
    );
  }

  callActionOnDevice(callAction, onerror);
}

function launchInterruptTransfer(hTransferParams, callback, onerror) {
  var callAction = function () {

    chrome.usb.claimInterface(
      getUsbDevice(),
      hTransferParams.interface,
      function () {

        if (chrome.runtime.lastError) {
          console.info(chrome.runtime.lastError);
          onerror("Error while claiming iface: "+chrome.runtime.lastError.message);
          return;
        } else {

          delete hTransferParams.interface;

          chrome.usb.interruptTransfer(
            getUsbDevice(),
            hTransferParams,
            function (transferResult) {
              if (chrome.runtime.lastError) {
                console.info(chrome.runtime.lastError);
                onerror("Error while asking interrupt transfer: "+chrome.runtime.lastError.message);
                return;
              } else {
              }
              chrome.usb.releaseInterface(
                getUsbDevice(),
                1,
                function () {
                  if (chrome.runtime.lastError) {
                    console.info(chrome.runtime.lastError);
                    onerror("Error while releasing interface: "+chrome.runtime.lastError.message);
                    return;
                  }
                  callback(transferResult);
                }
              );
            }
          );
        };
      }
    );
  }

  callActionOnDevice(callAction, onerror);
}


// This just calls some function (passed as parameter) making sure, that device is connected.
// Or not. It seems it works.
function callActionOnDevice(callAction, onerror) {
    if (top.hDevice) {
        callAction()
    } else {
        connectDigiUSB(callAction, onerror);
    }
}


// Sends byte using control transfer with request 0x09. DigiUSB expects that.
function sendByte(byte, callback) {

    var callAction = function () {

        var ab = new ArrayBuffer(1);

        chrome.usb.controlTransfer(
            top.hDevice,

            {direction: 'out', recipient: 'other', requestType: 'class', request: 0x09, value: 0x01, length:1, data: ab, index: byte},

            log2
        );
    };

    callActionOnDevice(callAction);
}


// Reads byte using control transfer with request type 0x01. DigiUSB.
function receiveByte(callback) {

    var callAction = function () {

        var ab = new ArrayBuffer(1);

        chrome.usb.controlTransfer(

            top.hDevice,

            {direction: 'in', recipient: 'other', requestType: 'class', request: 0x01, value: 0x01, length: 1, index: 100, data: ab.buffer},

            function(d) {
                if (chrome.runtime.lastError) { log(chrome.runtime.lastError) };
                var data = new Uint8Array(d.data);
                callback(data[0]);
            }

        )
    };

    callActionOnDevice(callAction);
}




// ugliness lies below :)

function log(msg) {
 // if (top.console && top.console.log) { console.log(msg, arguments)}
};
