/*
 * usb.c
 *
 *  Created on: 10 Apr 2016
 *      Author: Andrzej Szukiewicz
 */

#include "usb.h"
#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include "tools.h"

void initializeUSB() {
    cli();
    usbInit();
    usbDeviceDisconnect();

    uchar i;
    i = 0;
    while(--i){
        wdt_reset();
        _delay_ms(1);
    }
    usbDeviceConnect();
    sei();

}

/* This method is a base method for implementing USB function.
 * It - in it's base form - shows how to parse request data.
 */
USB_PUBLIC usbMsgLen_t usbFunctionSetup (uchar data[8]) {

  // First, we check whether it is OUT tranfser (host to device) or IN transfer (device to host).
  // Remember, that even the IN tansfers are initiated by host. Device on USB bus is expected to
  // not send anything unless asked for it.
  if ((((usbRequest_t*)(data))->bmRequestType & USBRQ_DIR_MASK) == USBRQ_DIR_HOST_TO_DEVICE) {
    // this is output transfer (device sends data to host).
    /* this is where things get complicated
     * if we do not need to fetch data sent from host to device, we will be ok with returning 0
     * from this routine. We still have possibility to use bRequest, wValue and wIndex for data
     * transfer. It still is 5 bytes! :)
     * We may also use the Data sent with request (up to 8 bytes), but it needs reconfigure the
     * driver and a little more complicated code structure, as we need to use usbFunctionWrite().
     * For that, we set USB_CFG_IMPLEMENT_FN_WRITE directive in usbconfig.h to 1 and we implement
     * the function. The enabling of the function makes our code bigger. About 100 bytes bigger.
     * In the template, the default state of the code is "use it!".
     * If you derive new project from it and do not need the functionality - just switch directive
     * to 0 and remove implementation. Will work. Hopefully.
     * For out transfer return USB_NO_MSG to have usbFunctionWrite called. Otherwise, return 0.
     * In a template, we parse data for control-out request 20. Otherwise, we don't care.
     */
     toggleLed();
     if (((usbRequest_t*)(data))->bRequest == 20) {
       return USB_NO_MSG;
     }
  } else {
    // this is input transfer.
    return USB_NO_MSG;
  }
  return 0;
}

/* usbFunctionWrite is called when the host wants to write
 * data to device.
 * (You have to return USB_NO_MSG from setup function!)
 * You have to put context somewhere, as in those function
 * you'll have no access to setup packet data.
 */
USB_PUBLIC uchar usbFunctionWrite(uchar *data, uchar len) {
  if (data[0] == 65) {
    toggleLed();
  }
  return 1;
}

/* if you want to put some extra data (WHOLE EIGHT BYTES!)
 * to the host, put it from usbFunctionRead.
 * You will need to put somewhere context for the function,
 * as generally you have no access to setup data here.
 */
USB_PUBLIC uchar usbFunctionRead(uchar *data, uchar len) {
  static uint8_t counter = 0;
  data[0]=counter++;
  data[1]=67;
  data[2]=68;
  data[3]=69;
  data[4]=70;
  data[5]=65;
  data[6]=66;
  data[7]=97;
  return 8;

}
