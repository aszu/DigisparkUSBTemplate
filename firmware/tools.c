/*
 * tools.c
 *
 *  Created on: 13 Apr 2016
 *      Author: Andrzej Szukiewicz
 */

#include <avr/io.h>
#include "tools.h"

/* This method toggles led. If you use PIN1 for something else,
 * the results may be at best "unpredictable".
 */
void toggleLed() {
  DDRB |= 0b10;
  PINB = 0b10;
}
