#include <avr/io.h>

#include "usbdrv/usbdrv.h"

#include <avr/boot.h>

#include <avr/wdt.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "usb.h"

/* Util functions declaration section.
 */
static inline void  codeInit();


/* Here comes the program
 */
int main () {

  codeInit();

  initializeUSB();

  while (1) {

    /* this is a "must have" of the main method - wdt_reset tells watchdog, that
     * a program is still alive. It must be called periodically (see wdt_enable
     * function and datasheet for details).
     * usbPoll is a requirement of vusb library. Should be called with the interval
     * not larger than 50ms.
     */
    wdt_reset();
    usbPoll();
  }
}

/* Util functions implementation
 */
static inline void codeInit() {
    wdt_enable(WDTO_8S);
}
