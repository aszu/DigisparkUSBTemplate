#ifndef _USB_H_TEMPLATE
#define _USB_H_TEMPLATE

#include "usbdrv/usbdrv.h"

void initializeUSB();

/* Most of the basic usb support is in this function
 * More details are available on the V-USB wiki:
 * 		http://vusb.wikidot.com/driver-api
 *
 */
USB_PUBLIC usbMsgLen_t usbFunctionSetup (uchar data[8]);

#endif
